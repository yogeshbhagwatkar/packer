#/bin/sh

# Update and install packages
sudo yum update -y
sudo yum install -y git
sudo yum install -y wget

# Download chef Package and install it.
sudo wget https://packages.chef.io/files/stable/chef/14.1.12/el/7/chef-14.1.12-1.el7.x86_64.rpm
sudo yum install -y chef-14.1.12-1.el7.x86_64.rpm

# create directory and give permission.
sudo mkdir -p /var/chef/cookbooks/
sudo chmod 777 /var/chef/
sudo chmod 777 /etc/

# Make a clone from Bitbucker for perticular repository and copy in proper path.
git clone https://yogeshbhagwatkar@bitbucket.org/yogeshbhagwatkar/limits.git
sudo mv /home/ec2-user/limits*  /var/chef/cookbooks/

git clone https://yogeshbhagwatkar@bitbucket.org/yogeshbhagwatkar/selinux.git
sudo mv  /home/ec2-user/selinux*  /var/chef/cookbooks/

git clone https://yogeshbhagwatkar@bitbucket.org/yogeshbhagwatkar/ops-base.git
sudo mv /home/ec2-user/ops-base*  /var/chef/cookbooks/

sudo chmod 666 /etc/bashrc

# Bash prompt customization
echo 'PS1="\n\[\e[32;1m\](\[\e[37;1m\]`hostname` - \u\[\e[32;1m\])-(\[\e[37;1m\]jobs:\j\[\e[32;1m\])-(\[\e[37;1m\]\w\[\e[32;1m\])\n(\[\[\e[37;1m\]! \!\[\e[32;1m\])-> \[\e[0m\]"' >> /etc/bashrc

echo "export PS1" >> /etc/bashrc

# Create solo.rb and node.json file with content for chef-solo run.
echo 'cookbook_path File.expand_path("../cookbooks", __FILE__)' > /var/chef/solo.rb
echo 'json_attribs File.expand_path("../node.json", __FILE__)' >> /var/chef/solo.rb
echo '{ "run_list": ["recipe[ops-base]"] }' >> /var/chef/node.json

# Run the chef-solo command for installation of Cookbook.
sudo chef-solo -c /var/chef/solo.rb

